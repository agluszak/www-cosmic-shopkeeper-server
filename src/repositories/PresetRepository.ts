import {AbstractRepository} from "./AbstractRepository";
import {Preset} from "../models/Preset";
import {PresetDetails} from "../models/PresetDetails";

export class PresetRepository extends AbstractRepository<Preset> {

    createTable(): Promise<void> {
        const sql = `
            CREATE TABLE IF NOT EXISTS presets (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                author TEXT NOT NULL,
                created INTEGER NOT NULL,
                description TEXT NOT NULL,
                content TEXT NOT NULL
            )`;
        return this.dao.run(sql, []);
    }

    create(preset: Preset): Promise<number> {
        return this.dao.insert(
            'INSERT INTO presets (name, author, created, description, content) VALUES (?, ?, ?, ?, ?)',
            [preset.name, preset.author, preset.created, preset.description, preset.content])
    }

    findByName(name: string): Promise<Preset | undefined> {
        let value = this.dao.get(`SELECT * FROM presets WHERE name = ?`, [name]) as Promise<Preset | undefined>;
        return value;
    }

    list(): Promise<PresetDetails[]> {
        return this.dao.all(`SELECT id, name, author, created, description FROM presets`, [])
    }
}