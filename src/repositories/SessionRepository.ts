import {Session} from "../models/Session";
import {AbstractRepository} from "./AbstractRepository";

export class SessionRepository extends AbstractRepository<Session> {

    createTable() {
        const sql = `
            CREATE TABLE IF NOT EXISTS sessions (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                username TEXT NOT NULL,
                sessionSecret TEXT NOT NULL,
                validUntil INTEGER NOT NULL
            )`;
        return this.dao.run(sql, [])
    }

    create(session: Session): Promise<number> {
        return this.dao.insert(
            'INSERT INTO sessions (username, sessionSecret, validUntil) VALUES (?, ?, ?)',
            [session.username, session.sessionSecret, session.validUntil])
    }

    findBySecret(sessionSecret: string): Promise<Session | undefined> {
        return this.dao.get(`SELECT * FROM sessions WHERE sessionSecret = ?`, [sessionSecret])
    }

    update(session: Session): Promise<number> {
        return this.dao.change(
            `UPDATE sessions SET username = ?, sessionSecret = ?, validUntil = ? WHERE id = ?`
            , [session.username, session.sessionSecret, session.validUntil, session.id])
    }


}