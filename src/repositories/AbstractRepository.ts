import {DAO} from "../utils/DAO";

export abstract class AbstractRepository<T> {
    protected dao: DAO;

    constructor(dao: DAO) {
        this.dao = dao
    }

    abstract createTable(): Promise<void>

    abstract create(obj: T): Promise<number>
}