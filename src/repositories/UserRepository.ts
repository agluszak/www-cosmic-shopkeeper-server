import {User} from "../models/User";
import {AbstractRepository} from "./AbstractRepository";

export class UserRepository extends AbstractRepository<User> {

    createTable() {
        const sql = `
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username TEXT NOT NULL,
      salt TEXT NOT NULL,
      hash TEXT NOT NULL)`;
        return this.dao.run(sql, [])
    }

    create(user: User) {
        return this.dao.insert(
            'INSERT INTO users (username, salt, hash) VALUES (?, ?, ?)',
            [user.username, user.salt, user.hash])
    }

    update(id: number, user: User) {
        return this.dao.change(
            `UPDATE users SET username = ?, salt = ?, hash = ? WHERE id = ?`,
            [user.username, user.salt, user.hash, id]
        )
    }

    delete(id: number) {
        return this.dao.change(
            `DELETE FROM users WHERE id = ?`,
            [id]
        )
    }

    findById(id: number): Promise<User | undefined> {
        return this.dao.get(
            `SELECT * FROM users WHERE id = ?`,
            [id])
    }

    findByUsername(username: string): Promise<User | undefined> {
        return this.dao.get(
            `SELECT * FROM users WHERE username = ?`,
            [username])
    }

    getAll(): Promise<User[]> {
        return this.dao.all(`SELECT * FROM users`, [])
    }
}