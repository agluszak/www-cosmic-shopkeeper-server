import {Encrypted} from "./Encrypted";

export interface User extends Encrypted {
    id: number;
    username: string;
}