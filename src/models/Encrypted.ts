export interface Encrypted {
    salt: string;
    hash: string;
}