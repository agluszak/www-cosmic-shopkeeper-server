export interface Session {
    id: number;
    username: string;
    sessionSecret: string;
    validUntil: number;
}