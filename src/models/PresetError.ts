export enum PresetError {
    PRESET_ALREADY_EXISTS = "Preset already exists",
    WRONG_TYPE = "Some field has wrong type",
    INCORRECT_JSON_STRUCTURE = "Incorrect JSON structure",
    MISSING_FIELD = "Some field is missing"
}