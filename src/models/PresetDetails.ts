export interface PresetDetails {
    id: number;
    name: string;
    author: string;
    created: number;
    description: string;
}