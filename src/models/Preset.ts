import {PresetDetails} from "./PresetDetails";

export interface Preset extends PresetDetails {
    content: string;
}