import {PresetRepository} from "../repositories/PresetRepository";
import {GameData} from "../models/GameData";
import {PresetDetails} from "../models/PresetDetails";
import {Preset} from "../models/Preset";

export class PresetService {
    private presetRepository: PresetRepository;

    constructor(presetRepository: PresetRepository) {
        this.presetRepository = presetRepository;
    }

    async createPreset(name: string, author: string, description: string, contentJson: GameData): Promise<PresetDetails | undefined> {
        if (await this.presetRepository.findByName(name) != undefined) {
            return undefined;
        }
        let created = Date.now();
        let content = JSON.stringify(contentJson);
        let preset: Preset = {id: 0, name, author, created, description, content};
        let id = await this.presetRepository.create(preset);
        preset.id = id;
        return preset;
    }

    async listPresets(): Promise<PresetDetails[]> {
        return await this.presetRepository.list();
    }

    async findPresetByName(name: string): Promise<Preset | undefined> {
        return await this.presetRepository.findByName(name);
    }
}