import {UserRepository} from "../repositories/UserRepository";
import {CryptoService} from "./CryptoService";
import {User} from "../models/User";

export class UserService {
    private userRepository: UserRepository;
    private cryptoService: CryptoService;

    constructor(userRepository: UserRepository, cryptoService: CryptoService) {
        this.userRepository = userRepository;
        this.cryptoService = cryptoService;
    }

    async createUser(username: string, password: string): Promise<User | undefined> {
        if (await this.userRepository.findByUsername(username) != undefined) {
            return undefined;
        }
        let encrypted = this.cryptoService.encrypt(password);
        let user: User = {id: 0, username, ...encrypted};
        let id = await this.userRepository.create(user);
        user.id = id;
        return user;
    }

    async checkPassword(username: string, password: string): Promise<boolean> {
        let user = await this.userRepository.findByUsername(username);
        if (user) {
            let passwordDecrypted = this.cryptoService.decrypt(user);
            return password == passwordDecrypted;
        } else {
            return false;
        }
    }
}