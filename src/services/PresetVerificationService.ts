export class PresetVerificationService {

    public verify(json: any): void {
        console.log(JSON.stringify(json));
        if (!json.time || !json.credits || !json.items || !json.planets || !json.starships) {
            throw "Top level field missing"
        }
        this.verifyNumber("time", json.time);
        this.verifyNumber("credits", json.credits);
        if (!Array.isArray(json.items)) {
            throw "items is not an array"
        }
        if (typeof json.planets != 'object') {
            throw "planets is not an object"
        }
        Object.keys(json.planets).forEach(name =>
            this.verifyPlanet(name, json.planets[name], json.items)
        );
        if (typeof json.starships != 'object') {
            throw "starships is not an object"
        }
        Object.keys(json.starships).forEach(name =>
            this.verifyStarship(name, json.starships[name], Object.keys(json.planets))
        );
    }

    private verifyNumber(name: string, number: any) {
        if (!Number.isInteger(number)) {
            throw name + " is not an integer"
        }
        if (number < 1) {
            throw name + " is not a positive number"
        }
    }

    private verifyAvailableItem(name: string, item: any) {
        if (!item.available || !item.buy_price || !item.sell_price) {
            throw "item " + name + " is missing a top level field"
        }
        this.verifyNumber(name + ".sell_price", item.sell_price);
        this.verifyNumber(name + ".buy_price", item.buy_price);
        this.verifyNumber(name + ".available", item.available);
    }

    private verifyPlanet(name: string, planet: any, items: string[]) {
        if (!planet.x || !planet.y || !planet.available_items) {
            throw "planet " + name + " is missing a top level field"
        }
        this.verifyNumber(name + ".x", planet.x);
        this.verifyNumber(name + ".y", planet.y);
        if (typeof planet.available_items != 'object') {
            throw "available_items is not an object"
        }
        Object.keys(planet.available_items).forEach(item => {
                if (!items.includes(item)) {
                    throw "unknown item " + item
                }
                this.verifyAvailableItem(item, planet.available_items[item])
            }
        )
    }

    private verifyStarship(name: string, starship: any, planets: string[]) {
        if (!starship.origin || !starship.cargo_hold_size) {
            throw "starship " + name + " is missing a top level field"
        }
        if (!planets.includes(starship.origin)) {
            throw "starship " + name + ": unknown planet " + starship.origin
        }
        this.verifyNumber("starship: " + name + ".cargo_hold_size", starship.cargo_hold_size);
    }
}