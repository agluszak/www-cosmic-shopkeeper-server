import {UserService} from "./UserService";
import {SessionRepository} from "../repositories/SessionRepository";
import * as crypto from "crypto";
import {Session} from "../models/Session";

export class SessionService {
    static sessionDuration = 1;
    static secretLength = 64;
    private userService: UserService;
    private sessionRepository: SessionRepository;

    constructor(userService: UserService, sessionRepository: SessionRepository) {
        this.userService = userService;
        this.sessionRepository = sessionRepository;
    }

    async startSession(username: string, password: string): Promise<Session | undefined> {
        if (await this.userService.checkPassword(username, password)) {
            let deadline = Date.now() + SessionService.sessionDuration * 60000;
            let secret = crypto.randomBytes(SessionService.secretLength).toString('hex');
            let session: Session = {
                id: 0,
                sessionSecret: secret,
                validUntil: deadline,
                username
            };
            let id = await this.sessionRepository.create(session);
            session.id = id;
            return session;
        } else {
            return undefined;
        }
    }

    async validateSession(username: string, secret: string): Promise<boolean> {
        let session = await this.sessionRepository.findBySecret(secret);
        if (session) {
            return session.validUntil > Date.now();
        } else {
            return false;
        }
    }

    async finishSession(secret: string): Promise<void> {
        let session = await this.sessionRepository.findBySecret(secret);
        if (session) {
            session.validUntil = Date.now() - 1;
            await this.sessionRepository.update(session);
        }
    }
}