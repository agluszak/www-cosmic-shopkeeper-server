import * as crypto from "crypto";
import {Encrypted} from "../models/Encrypted";

export class CryptoService {
    private algorithm = 'aes-256-cbc';
    private key = "SuperTajneHaslo1SuperTajneHaslo1";

    encrypt(text: string): Encrypted {
        let iv = crypto.randomBytes(16);
        let cipher = crypto.createCipheriv(this.algorithm, Buffer.from(this.key), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return {salt: iv.toString('hex'), hash: encrypted.toString('hex')};
    }

    decrypt(encrypted: Encrypted) {
        let iv = Buffer.from(encrypted.salt, 'hex');
        let encryptedText = Buffer.from(encrypted.hash, 'hex');
        let decipher = crypto.createDecipheriv(this.algorithm, Buffer.from(this.key), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
}