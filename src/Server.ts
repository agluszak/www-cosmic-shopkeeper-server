import {SessionRepository} from "./repositories/SessionRepository";
import {UserRepository} from "./repositories/UserRepository";
import {SessionService} from "./services/SessionService";
import {UserService} from "./services/UserService";
import {CryptoService} from "./services/CryptoService";
import {DAO} from "./utils/DAO";
import * as express from 'express';
import {Express} from 'express';
import * as cookieParser from "cookie-parser";
import {Session} from "./models/Session";
import * as cors from 'cors';
import {PresetService} from "./services/PresetService";
import {PresetRepository} from "./repositories/PresetRepository";
import {PresetVerificationService} from "./services/PresetVerificationService";
import {NextFunction} from "connect";

export class Server {
    private dao: DAO;

    private sessionRepository: SessionRepository;
    private userRepository: UserRepository;
    private presetRepository: PresetRepository;

    private sessionService: SessionService;
    private userService: UserService;
    private cryptoService: CryptoService;
    private presetService: PresetService;
    private presetVerificationService: PresetVerificationService;

    private app: Express;

    private clientAddress = "http://localhost:3030";
    private port = 3031;

    constructor() {
        this.dao = new DAO("shopkeeper.db");

        this.sessionRepository = new SessionRepository(this.dao);
        this.userRepository = new UserRepository(this.dao);
        this.presetRepository = new PresetRepository(this.dao);

        this.cryptoService = new CryptoService();
        this.userService = new UserService(this.userRepository, this.cryptoService);
        this.sessionService = new SessionService(this.userService, this.sessionRepository);
        this.presetService = new PresetService(this.presetRepository);
        this.presetVerificationService = new PresetVerificationService();

        this.app = express();
    }

    errorMiddleware(error: any, request: express.Request, response: express.Response, next: NextFunction) {
        if (error.message) {
            response.status(400).send({error: error.message})
        } else {
            response.status(500).send({error: "Unknown error"})
        }
    }

    async start() {
        await this.sessionRepository.createTable();
        await this.userRepository.createTable();
        await this.presetRepository.createTable();

        this.app.use(express.urlencoded({extended: true}));

        this.app.use(express.json());
        this.app.use(cookieParser());
        this.app.use(cors({credentials: true, origin: this.clientAddress}));
        this.app.use(this.errorMiddleware);

        this.app.post('/users/register', async (req, res) => {
            let username = req.body.username;
            let password = req.body.password;
            if (username && password) {
                let user = await this.userService.createUser(username, password);
                if (user) {
                    res.status(201).send({ok: "Registered!"});
                } else {
                    res.status(400).send({error: "User already exists"});
                }
            } else {
                res.status(400).send({error: "No username or password provided"});
            }
        });

        this.app.post('/users/login', async (req, res) => {
            let username = req.body.username;
            let password = req.body.password;
            if (username && password) {
                let session = await this.sessionService.startSession(username, password);
                if (session) {
                    res.cookie("session", session, {httpOnly: true});
                    res.send({ok: "Logged in"});
                } else {
                    res.status(401).send({error: "Wrong password or username"});
                }
            } else {
                res.status(400).send({error: "No username or password provided"});
            }
        });

        this.app.post('/users/logout', async (req, res) => {
            let session = req.cookies.session as Session | undefined;
            if (session && session.sessionSecret && session.username) {
                res.clearCookie("session");
                let authorized = await this.sessionService.validateSession(session.username, session.sessionSecret);
                if (authorized) {
                    await this.sessionService.finishSession(session.sessionSecret);
                    res.send({ok: "Logged out!"});
                } else {
                    res.send({ok: "Already logged out"});
                }
            } else {
                res.status(401).send({error: "No session"});
            }
        });

        this.app.get('/presets/list', async (req, res) => {
            let presets = await this.presetService.listPresets();
            res.send({ok: presets});
        });

        this.app.get('/presets/get/:name', async (req, res) => {
            console.log(req.params["name"]);
            let preset = await this.presetService.findPresetByName(req.params["name"]);
            if (!preset) {
                res.status(404).send({error: "Preset not found"});
            } else {
                res.send({ok: preset});
            }
        });

        this.app.post('/presets/upload', async (req, res) => {
            let session = req.cookies.session as Session | undefined;
            if (session && session.sessionSecret && session.username) {
                let authorized = await this.sessionService.validateSession(session.username, session.sessionSecret);
                if (authorized) {
                    if (!req.body.name || !req.body.description || !req.body.content) {
                        res.status(400).send({error: "Missing fields"});
                    } else {
                        try {
                            this.presetVerificationService.verify(JSON.parse(req.body.content));
                            let result = await this.presetService.createPreset(req.body.name, session.username, req.body.description, req.body.content);
                            if (result) {
                                res.send({ok: "Preset uploaded"});
                            } else {
                                res.status(400).send({error: "Preset already exists"});
                            }
                        } catch (e) {
                            res.status(400).send({error: e})
                        }
                    }
                } else {
                    res.status(401).send({error: "Invalid session"});
                }
            } else {
                res.status(401).send({error: "No session"});
            }
        });

        this.app.listen(this.port, () => {
            console.log('app listening on port: ' + this.port);
        });

    }

}